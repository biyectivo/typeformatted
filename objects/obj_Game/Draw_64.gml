switch(example) {
	case 0:
		draw_set_font(-1);
		draw_set_color(c_white);
		draw_text(10, 30, "The quick brown fox jumps over the lazy dog 0123456789");
		draw_set_font(fnt_Default);
		draw_text(10, 60, "The quick brown fox jumps over the lazy dog 0123456789");
		break;
	case 1:

		var _txtTitle = type_formatted(room_width/2, 10, "[fnt_TypeFormatted][c_white][fa_center][fa_top]TypeFormatted");
		type_formatted(_txtTitle.bbox_width, _txtTitle.bbox_height, "[fa_bottom][fa_right][fnt_Consolas][scale,0.1][c_lime]"+TYPE_FORMATTED_VERSION);
		var _txtDesc = type_formatted(room_width/2, 250, "[fnt_TypeFormatted][scale,0.2][c_white][fa_center][fa_middle]The [c_dksalmon]amateur developer's[c_white] version of JujuAdams's fabulous [#4444EE]Scribble[c_white] asset.", true, true, 0, 0, 10);
		
		if (_txtDesc.getCurrentCharacter() % 2 == 0) {
			var _func = function() {
				audio_play_sound(snd_Typewriter, 1, false);
			};
		}
		else {
			var _func = function() {};
		}

		_txtDesc.setTypewriterAction(_func);
		
		if (keyboard_check_pressed(vk_enter)) {
			_txtDesc.endTypewriter();
		}
		if (_txtDesc.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {
			var _txtMotto = type_formatted(room_width/2, 350, "[fnt_DroidSans][scale,0.2][c_white][fa_center][fa_middle][chr,34]Bad performance, but it works in HTML5![chr,34]", true, true, 0, 0, 2);
			if (_txtMotto.getTypewriterStatus() == TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED) {
				type_formatted(room_width/2, 450, "[fnt_Consolas][scale,0.1][c_gray][fa_center][fa_bottom]by José Alberto Bonilla Vera (biyectivo)  :  https://www.biyectivo.com/typeformatted  :  biyectivo@gmail.com  :  biyectivo#2771");
			}
		}
		
		type_formatted(room_width/2, 540, "[fnt_TypeFormatted][scale,0.2][fa_bottom][fa_center][c_blue]Press \\]ENTER\\[ to [alpha,0.8][angle,45]toggle[angle,0][alpha,1] between examples",true, true, 10);
		break;
	case 2:
		
		var _basestyle = "[fnt_TypeFormatted][c_white][scale,0.2][fa_left][fa_top]";
		var _x0 = 10
		var _offset = 0;
		for (var _i=32; _i<126; _i++) {
			var _txtASCII = type_formatted(_x0+_offset, 10, _basestyle+"[chr,"+string(_i)+"]");
			_offset = _offset + _txtASCII.bbox_width;
		}	
		
		break;

	default:
		break;
}

type_formatted(room_width-10, room_height-10, "[fnt_Consolas][c_lime][fa_right][fa_bottom][scale,0.2]FPS: "+string_format(fps_real,4,0), true, false);