{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 1,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Typewriter.wav",
  "duration": 0.274667,
  "parent": {
    "name": "Sounds",
    "path": "folders/TypeFormatted Example Project/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Typewriter",
  "tags": [],
  "resourceType": "GMSound",
}