/// Global variables and data structures
#macro TYPE_FORMATTED_DEFAULT_ALPHA 1.0
#macro TYPE_FORMATTED_DEFAULT_COLOR c_black
#macro TYPE_FORMATTED_DEFAULT_HALIGN fa_left
#macro TYPE_FORMATTED_DEFAULT_VALIGN fa_top
#macro TYPE_FORMATTED_DEFAULT_XSCALE 1
#macro TYPE_FORMATTED_DEFAULT_YSCALE 1
#macro TYPE_FORMATTED_DEFAULT_FONT -1 // STRONGLY RECOMMENDED to substitute with your most used font. Use -1 for Arial 12, GMS2's default font, but beware this causes additional texture swaps
#macro TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE 0.05
#macro TYPE_FORMATTED_DEBUG_BBOX false
#macro TYPE_FORMATTED_DEBUG_WARNINGS true
#macro TYPE_FORMATTED_DEBUG_NOTES false
#macro TYPE_FORMATTED_DEBUG_VERBOSE false
#macro TYPE_FORMATTED_AUTOFLUSH true
#macro TYPE_FORMATTED_CACHE_SIZE_FLUSH 150
#macro TYPE_FORMATTED_VERSION "0.6 beta"


enum TYPE_FORMATTED_TYPEWRITER {
	TYPEWRITER_DISABLED,
	TYPEWRITER_PAUSED,
	TYPEWRITER_RUNNING,
	TYPEWRITER_ENDED	
}



function fnc_draw_text_ext_transformed(_array) {
	var _x = _array[0];
	var _y = _array[1];
	var _string = _array[2];
	var _sep = _array[3];
	var _w = _array[4];
	var _xscale = _array[5]; 
	var _yscale = _array[6];
	var _angle = _array[7];
	
	if (_sep == -1 && _w == -1 && _xscale == 1 && _yscale == 1 && _angle == 0) {
		draw_text(_x, _y, _string);
	}
	else if (_xscale == 1 && _yscale ==1 && _angle == 1) {
		draw_text_ext(_x,_y,_string,_sep,_w);
	}
	else if (_sep == -1 && _w == -1) {
		draw_text_transformed(_x, _y, _string, _xscale, _yscale, _angle);
	}
	else {
		draw_text_ext_transformed(_x,_y,_string,_sep,_w,_xscale,_yscale,_angle);
	}
}

function fnc_draw_set_alpha(_arg) {
	if (is_array(_arg)) {
		var _alpha = _arg[0];
	}
	else {
		var _alpha = _arg;
	}
	
	if (abs(draw_get_alpha() - _alpha) > TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_set_alpha(_alpha);	
	}
}

function fnc_draw_set_color(_arg) {
	if (is_array(_arg)) {
		var _color_string = _arg[0];
	}
	else {
		var _color_string = _arg;
	}
	
	var _col = fnc_parse_color(_color_string);
	if (draw_get_color() != _col) {
		draw_set_color(_col);
	}
}

function fnc_parse_color(_color_string) {
	if (is_real(_color_string)) {
		return _color_string;
	}
	else if (_color_string == "c_random") {
		return make_color_rgb(irandom_range(0,255), irandom_range(0,255), irandom_range(0,255));
	}
	else if (!is_undefined(ds_map_find_value(global.tf_colors, _color_string))) {
		return global.tf_colors[? _color_string];
	}
	else if (string_copy(_color_string, 1, 1) == "$") {
		return base_convert(string_copy(_color_string,2,6), 16, 10);
	}
	else if (string_copy(_color_string, 1, 1) == "#") {
		return base_convert(string_reverse(string_copy(_color_string,2,6)), 16, 10);		
	}
	else {
		return -1;	
	}
}

function fnc_draw_set_font(_arg) {
	if (is_array(_arg)) {
		var _font = _arg[0];
	}
	else {
		var _font = _arg;
	}
	if (draw_get_font() != _font) {		
		draw_set_font(_font);
	}
}



function type_formatted_draw(_x, _y, _string/*, _cache*/) {	
	
	var _cache = true;
	if (argument_count > 3) {
		_cache = argument[3];
	}
	
	// ======================
	// Perform final draw
	// ======================
	
	// Reset defaults
	var _curr_scale_x = TYPE_FORMATTED_DEFAULT_XSCALE;
	var _curr_scale_y = TYPE_FORMATTED_DEFAULT_YSCALE;
	var _curr_angle = 0;
	var _curr_blend = c_white;
	var _curr_sep = -1;
	var _curr_wrap = -1;
	
	fnc_draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
	fnc_draw_set_alpha(TYPE_FORMATTED_DEFAULT_ALPHA);
	
	// Assumes it exists
	var _structString = fnc_TypeFormatted_FindParsedString(_string);
		
	if (_structString.halign == fa_center) {
		var _curr_x = _x - _structString.bbox_width/2;
	}
	else if (_structString.halign == fa_right) {
		var _curr_x = _x - _structString.bbox_width;
	}
	else {
		var _curr_x = _x;
	}
		
	draw_set_halign(fa_left);
	draw_set_valign(_structString.valign);
	
	if (TYPE_FORMATTED_DEBUG_NOTES) {
		show_debug_message("=== [TypeFormatted/type_formatted_draw]: NOTE: Drawing parsed item from cache: "+_string);
	}
			
	// Draw the elements corresponding to the currently parsed string
	var _n = array_length(_structString.parsed_string_array);
	var _s = 0; // index for sprite array
	var _i = 0; // index for element arays
	var _character_limit = _structString.typewriter_delay == -1 ? 99999999 : _structString.current_character;
	var _count = 0;
	var _next_item = true;
	
	while (_next_item && _i<_n && _count < _character_limit) {
		var _parameter_array = _structString.parameter_array[_i];
		var _element = _structString.parsed_string_array[_i];
		if (string_copy(_element, 1, 1) == "@") { // Process control codes for sprites, etc.
			if (string_copy(_element, 1, 7) == "@sprite") {
				var _sprite_index = _structString.sprite_array[_s];
				var _initial_subimg_array = _structString.current_subimg[_s];
				
				_curr_scale_x = real(_parameter_array[0]);
				_curr_scale_y = real(_parameter_array[1]);
				_curr_angle = real(_parameter_array[2]);				
				_curr_blend = fnc_parse_color(string_replace_all(_parameter_array[3], " ", ""));
				_curr_sep = real(_parameter_array[4]);
				_curr_wrap = real(_parameter_array[5]);
		
				// Determine offsets for non top-left sprites
				var _offset_x = sprite_get_xoffset(_sprite_index) * _curr_scale_x;
				var _offset_y = sprite_get_yoffset(_sprite_index) * _curr_scale_y;				
				
				
				// Determine alignment for sprites
				if (draw_get_valign() == fa_top) {							
					var _y1sprite = _y + _offset_y;						
				}
				else if (draw_get_valign() == fa_bottom) {
					var _y1sprite = _y + _offset_y - sprite_get_height(_sprite_index)*_curr_scale_y;
				}
				else {
					var _y1sprite = _y + _offset_y - sprite_get_height(_sprite_index)*_curr_scale_y/2;
				}
				
				// Blink
				if (_structString.blink_speed > 0 && _structString.blink_alarm < 0) { // Blink
					var _final_alpha = 0;
				}
				else {
					var _final_alpha = draw_get_alpha();
				}
				
				draw_sprite_ext(_sprite_index, _initial_subimg_array, _curr_x + _offset_x, _y1sprite, _curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _final_alpha);
				_curr_x = _curr_x + sprite_get_width(_sprite_index) * _curr_scale_x;			
				_s++;
				_count++;
				_next_item = true;
			}
			else { // Process control codes for draw alpha/font/etc.
				
				var _function = string_copy(_element, 2, string_length(_element));
				
				switch (_function) {
					case "draw_set_alpha":
						fnc_draw_set_alpha(_parameter_array);
						break;
					case "draw_set_color":
						fnc_draw_set_color(_parameter_array);
						break;
					case "draw_set_font": 
						fnc_draw_set_font(_parameter_array);
						break;
					default:
						// Something was called and the switch statement that processes the formatting changes did not account for it
						break;
				}
								
				_next_item = true;
			}
		}
		else { // Process draw text
			_curr_scale_x = real(_parameter_array[0]);
			_curr_scale_y = real(_parameter_array[1]);
			_curr_angle = real(_parameter_array[2]);
			_curr_blend = fnc_parse_color(string_replace_all(_parameter_array[3], " ", ""));
			_curr_sep = real(_parameter_array[4]);
			_curr_wrap = real(_parameter_array[5]);	
			
			
			var _max_string_chars = _character_limit - _count;
			var _string_length = string_length(_element);
			var _final_chars_to_consider = min(_max_string_chars, _string_length);
			
			var _final_string = string_copy(_element, 1, _final_chars_to_consider);
			var _parameter_array = [_curr_x, _y, _final_string, _curr_sep, _curr_wrap, _curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend];
			
			// Blink
			if (_structString.blink_speed > 0 && _structString.blink_alarm < 0) { // Blink
				draw_set_alpha(0);
			}
			
			fnc_draw_text_ext_transformed(_parameter_array);
			
			_curr_x = _curr_x + string_width(_element) * _curr_scale_x;		
			_count = _count + _final_chars_to_consider;
			_next_item = (_final_chars_to_consider == _string_length);
		}
		if (_next_item) {
			_i++;	
		}		
	}
	


	if (TYPE_FORMATTED_DEBUG_VERBOSE) {
		show_debug_message("=== [TypeFormatted/type_formatted_draw]: INFO: Finalized drawing "+_string);
	}
	
	
	
	
	
	// Debug bbox
	if (TYPE_FORMATTED_DEBUG_BBOX) {
		draw_set_halign(fa_center);
		draw_set_valign(fa_bottom);
		draw_set_font(-1);
		draw_set_color(c_black);
		draw_set_alpha(1);
				
		var _bbox_coords = _structString.bbox(_x,_y);
				
		draw_circle_color(_bbox_coords[0], _bbox_coords[1], 2, c_green, c_green, false);
		draw_text(_bbox_coords[0], _bbox_coords[1], string(_bbox_coords[0])+","+string(_bbox_coords[1]));
		draw_circle_color(_bbox_coords[0], _bbox_coords[3], 2, c_dkgray, c_dkgray, false);
		draw_text(_bbox_coords[0], _bbox_coords[3]+20, string(_bbox_coords[0])+","+string(_bbox_coords[3]));
		draw_line_color(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[1], c_gray, c_gray);
		draw_circle_color(_bbox_coords[2], _bbox_coords[1], 2, c_dkgray, c_dkgray, false);
		draw_text(_bbox_coords[2], _bbox_coords[1], string(_bbox_coords[2])+","+string(_bbox_coords[1]));
		draw_circle_color(_bbox_coords[2], _bbox_coords[3], 2, c_blue, c_blue, false);
		draw_text(_bbox_coords[2], _bbox_coords[3]+20, string(_bbox_coords[2])+","+string(_bbox_coords[3]));
		draw_line_color(_bbox_coords[0], _bbox_coords[3], _bbox_coords[2], _bbox_coords[3], c_gray, c_gray);
		draw_circle_color(_x, _y, 7, c_red, c_red, false);
		draw_set_alpha(0.2);
		draw_set_color(c_blue);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
		if (TYPE_FORMATTED_DEBUG_VERBOSE) {
			show_debug_message("=== [TypeFormatted/type_formatted_draw]: INFO: Anchor point (provided x,y): "+string(_x)+","+string(_y)+"; bbox coordinates: "+string(_bbox_coords[0])+","+string(_bbox_coords[1])+" "+string(_bbox_coords[2])+","+string(_bbox_coords[1])+" "+string(_bbox_coords[0])+","+string(_bbox_coords[3])+" "+string(_bbox_coords[2])+","+string(_bbox_coords[3]));
		}
	}
	
	if (!_cache) {
		fnc_TypeFormatted_RemoveParsedStringFromCache(_string);
	}
}

function type_formatted_create(_x, _y, _string /*, _blink_speed, _start_character, _speed */) {
	
	var _blink_speed = 0;
	if (argument_count > 3) {
		_blink_speed = argument[3];
	}
	var _initial_character = 0; // All characters by default
	if (argument_count > 4) {
		_initial_character = argument[4];
	}
	var _typewriter_delay = -1; // No animation by default
	if (argument_count > 5) {
		_typewriter_delay = argument[5];
	}
	
	if (TYPE_FORMATTED_DEBUG_VERBOSE) {		
		show_debug_message("=== [TypeFormatted/type_formatted_create]: INFO: Parsing string: "+_string);
	}

	
	var _inside_text_split = noone;
	
	try {		
		if (string_count("[",_string)-string_count("\\[",_string) != string_count("]",_string)-string_count("\\]",_string)) {
			throw("=== [type_formatted/type_formatted_create]: ERROR: tag open/close mismatch");
		}
		else {			
			// TODO: fix drawing rotated text/sprites using the rotated baseline
			// TODO: Factor rotation into width/height of bbox
			// TODO: Check for nested []
			// TODO: Improve data validation
			
			var _n = string_length(_string);
			
			var _inside = false;
			var _inside_text = "";
			var _text = "";
			var _escape = false;
			
			var _parsed_array = [];
			var _sprite_array = [];
			
			var _initial_subimg_array = [];
			var _speed_array = [];
			var _parameter_array = [];
			
			var _curr_width = 0;
			var _curr_height = 0;
			
			// Scales and angles, apply to sprites and text
			var _curr_scale_x = 1.0;
			var _curr_scale_y = 1.0;
			var _curr_angle = 0.0;
			var _curr_blend = "c_white";
			var _curr_sep = -1;
			var _curr_wrap = -1;
			
			var _curr_halign = fa_left;
			var _curr_valign = fa_top;
			
			fnc_draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
			
			// Process string
			for (var _i=1; _i<=_n; _i++) {
				var _chr = string_copy(_string, _i, 1);
				if (_chr == "\\" && !_escape) { // Enable escape character
					_escape = true;
				}
				else if (_escape) { // Process escaped character as if it is text					
					_text = _text + _chr;
					_curr_width = _curr_width + string_width(_chr)*_curr_scale_x;
					_curr_height = max(_curr_height, string_height(_chr)*_curr_scale_y);
					
					_escape = false;
				}
				else if (_chr == "[") { // Go inside for a tag					
					_inside = true;	
					
					// Add current text if not already there and not empty					
					if (_text != "") {	
						array_push(_parsed_array, _text);
						array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
					}
					_text = "";
				}
				else if (_chr == "]") { // Process inside tag, keep only the first part (ignore arguments after commas)
					_inside = false;
					var _full_inside_text = string_replace_all(_inside_text, " ", ""); 
					var _inside_text_split = string_to_list(_full_inside_text, ",", true);
					var _n_split = ds_list_size(_inside_text_split);
					
					_inside_text = _inside_text_split[|0];
					// See if first element of inside text tag matches an asset
					var _idx = asset_get_index(_inside_text);
					var _idxtype = asset_get_type(_inside_text);
					
					if (TYPE_FORMATTED_DEBUG_VERBOSE) {
						show_debug_message("=== [TypeFormatted/type_formatted_create]: INFO: Trying to find assets for tag ["+_inside_text+"] with index "+string(_idx)+" and type "+string(_idxtype));
					}
					
					if (string_length(_inside_text) == 0) { // Reset default alpha, color, scalex, scaley and angle - NOTE, no alignment reset						
						// Reset to defaults
						_curr_scale_x = 1.0;
						_curr_scale_y = 1.0;
						_curr_angle = 0.0;
						_curr_blend = "c_white";
						_curr_wrap = -1;
						_curr_sep = -1;						
						// Add font reset
						array_push(_parsed_array, "@TypeFormatted_Defaults");
						array_push(_parameter_array, []);
						draw_set_font(TYPE_FORMATTED_DEFAULT_FONT); // needed for bbox
					}
					else if (string_copy(_inside_text, 1, 3) == "chr") { // include ASCII character
						var _txtcode = chr(real(_inside_text_split[|1]));
						_curr_width = _curr_width + string_width(_txtcode)*_curr_scale_x;
						_curr_height = max(_curr_height, string_height(_txtcode)*_curr_scale_y);
						array_push(_parsed_array, _txtcode);
						array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);					
					
					}					
					else if (string_copy(_inside_text, 1, 1) == "#" || string_copy(_inside_text, 1, 1) == "$") { // Change color, custom
						array_push(_parsed_array, "@draw_set_color");
						array_push(_parameter_array, [_inside_text]);
					}
					else if (string_copy(_inside_text, 1, 8) == "c_random") {
						if (TYPE_FORMATTED_DEBUG_VERBOSE) {
							show_debug_message("=== [TypeFormatted/type_formatted_create]: INFO: Random color parsed");
						}
						array_push(_parsed_array, "@draw_set_color");
						array_push(_parameter_array, [_inside_text]);	
					}
					else if (string_copy(_inside_text, 1, 2) == "c_") { // Change color, built-in constant
						if (!is_undefined(ds_map_find_value(global.tf_colors, _inside_text))) {
							array_push(_parsed_array, "@draw_set_color");
							array_push(_parameter_array, [_inside_text]);	
						}						
						else {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such color constant exists");	
							}
						}
					}
					else if (string_copy(_inside_text, 1, 5) == "angle") { // Change angle ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for angle");	
							}
						}
						else {
							_curr_angle = _inside_text_split[|1] % 360;
						}						
					}
					else if (string_copy(_inside_text, 1, 5) == "blend") { // Change blend color ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for blend");	
							}
						}
						else {
							_curr_blend = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 3) == "sep") { // Change sep ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for sep");	
							}
						}
						else {
							_curr_sep = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 4) == "wrap") { // Change wrap ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for wrap");	
							}
						}
						else {
							_curr_wrap = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 5) == "alpha") { // Change alpha
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for alpha");	
							}
						}
						else {
							array_push(_parsed_array, "@draw_set_alpha");
							array_push(_parameter_array, [clamp(real(_inside_text_split[|1]), 0, 1)]);
						}
					}
					else if (string_copy(_inside_text, 1, 5) == "scale") { // Change scaling
						if (_n_split < 2 || _n_split > 3) {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid values for scaling");	
							}
						}
						else {
							_curr_scale_x = real(_inside_text_split[|1]);
							if (_n_split == 2) {
								_curr_scale_y = _curr_scale_x;
							}
							else {
								_curr_scale_y = real(_inside_text_split[|2]);
							}
						}
					}					
					else if (string_copy(_inside_text, 1, 3) == "fa_") { // Change alignment
						if (_inside_text == "fa_left" || _inside_text == "fa_center" || _inside_text == "fa_right") {
							if (!is_undefined(ds_map_find_value(global.tf_halignments, _inside_text))) {
								_curr_halign = ds_map_find_value(global.tf_halignments, _inside_text);
							}
						}
						else if (_inside_text == "fa_top" || _inside_text == "fa_middle" || _inside_text == "fa_bottom") {
							if (!is_undefined(ds_map_find_value(global.tf_valignments, _inside_text))) {								
								_curr_valign = ds_map_find_value(global.tf_valignments, _inside_text);
							}
						}
						else {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such alignment value exists");	
							}
						}							
					}
					else if (_idx != -1) {
						if (TYPE_FORMATTED_DEBUG_VERBOSE) {
							show_debug_message("=== [TypeFormatted/type_formatted_create]: INFO: Confirmed, ["+_inside_text+"] is an asset of type "+string(_idxtype));
						}
						
						if (_idxtype == asset_sprite) {							
							
							if (sprite_exists(_idx)) { // Generate sprite element
								if (_n_split > 3) {
									if (TYPE_FORMATTED_DEBUG_WARNINGS) {
										show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', invalid parameters");	
									}
								}
								else {									
									if (_n_split == 1) {
										var _subimg = 0;
										var _animation_speed = 0;
									}
									else if (_n_split == 2) {
										var _subimg = real(_inside_text_split[|1]);
										var _animation_speed = 0;
									}
									else if (_n_split == 3) {
										var _subimg = real(_inside_text_split[|1]);
										if (string_length(_inside_text_split[|2]) == 0) {
											var _animation_speed = sprite_get_speed(_idx);
										}
										else {
											var _animation_speed = real(_inside_text_split[|2]);	
										}										
									
									}
									var _sprite_width = sprite_get_width(_idx)*_curr_scale_x;
									var _sprite_height = sprite_get_height(_idx)*_curr_scale_y;
									_curr_width = _curr_width + _sprite_width;
									_curr_height = max(_curr_height, _sprite_height);
								
									// Add sprite arrays
									array_push(_parsed_array, "@sprite");
									array_push(_sprite_array, _idx);									
									array_push(_initial_subimg_array, _subimg);
									array_push(_speed_array, _animation_speed);
									array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
								}
								
							}
						}
						else if (_idxtype == asset_font) {							
							if (font_exists(_idx)) { // Change font ***
								array_push(_parsed_array, "@draw_set_font");
								array_push(_parameter_array, [_idx]);
								draw_set_font(_idx); // needed for bbox
							}
						}						
						else {
							if (TYPE_FORMATTED_DEBUG_WARNINGS) {
								show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such sprite or font exists");
							}
						}
					}
					else {
						if (TYPE_FORMATTED_DEBUG_WARNINGS) {
							show_debug_message("=== [TypeFormatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such asset exists and no valid keyword was found");
						}
					}
					_inside_text = "";
				}
				else if (_inside) { // Save inside tag into a string variable
					_inside_text = _inside_text + _chr;
				}
				else { // Save text into a variable, write text if it's the last character of the string					
					_text = _text + _chr;
					_curr_width = _curr_width + string_width(_chr)*_curr_scale_x;
					_curr_height = max(_curr_height, string_height(_chr)*_curr_scale_y);
					
					// Add last text element
					
					if (_i==_n) {						
						if (_text != "") {								
							// Add current text if not already there and not empty										
							array_push(_parsed_array, _text);
							array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
						}
						_text = "";						
					}
				}
			}
			
			
			
			// Clean
			if (ds_exists(_inside_text_split, ds_type_list)) {
				ds_list_destroy(_inside_text_split);
			}
			
			
			
		}
	}

	var _result = new global.TypeFormatted_ParsedElement(_parsed_array, _sprite_array, _initial_subimg_array, _speed_array, _parameter_array, _curr_width, _curr_height, _curr_halign, _curr_valign, _initial_character, _typewriter_delay, _blink_speed);
	ds_map_add(	global.TypeFormatted_parsedElementMap,
				_string, 
				_result);
				
	if (TYPE_FORMATTED_DEBUG_NOTES) {
		show_debug_message("=== [TypeFormatted/type_formatted_create]: NOTE: Parsed "+_string + " / Total width="+string(_curr_width)+" *** Total height "+string(_curr_height));
	}

	
}

function fnc_TypeFormatted_FindParsedString(_string_id) {
	return global.TypeFormatted_parsedElementMap[? _string_id];
}

function fnc_TypeFormatted_RemoveParsedStringFromCache(_string_id) {
	if (ds_map_exists(global.TypeFormatted_parsedElementMap, _string_id)) {
		ds_map_delete(global.TypeFormatted_parsedElementMap, _string_id);	
	}
}

function fnc_TypeFormatted_Defaults() {
	// No alignment reset
	// Scale, angle, blend and wrap are reset at the corresponding step
	if (draw_get_font() != TYPE_FORMATTED_DEFAULT_FONT) {
		draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
	}
	if (draw_get_color() != TYPE_FORMATTED_DEFAULT_COLOR) {
		draw_set_color(TYPE_FORMATTED_DEFAULT_COLOR);
	}
	if (draw_get_alpha() != TYPE_FORMATTED_DEFAULT_ALPHA) {
		draw_set_alpha(TYPE_FORMATTED_DEFAULT_ALPHA);
	}
}


function base_convert(_string_number, _old_base, _new_base) {
	var number, oldbase, newbase, out;
	number = _string_number;
    //number = string_upper(_string_number);
    oldbase = _old_base;
    newbase = _new_base;
    out = "";
 
    var len, tab;
    len = string_length(number);
    tab = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    var i, num;
    for (i=0; i<len; i+=1) {
        num[i] = string_pos(string_char_at(number, i+1), tab) - 1;
    }
 
    do {
        var divide, newlen;
        divide = 0;
        newlen = 0;
        for (i=0; i<len; i+=1) {
            divide = divide * oldbase + num[i];
            if (divide >= newbase) {
                num[newlen] = divide div newbase;
                newlen += 1;
                divide = divide mod newbase;
            } else if (newlen  > 0) {
                num[newlen] = 0;
                newlen += 1;
            }
        }
        len = newlen;
        out = string_char_at(tab, divide+1) + out;
    } until (len == 0);
 
    return out;
}


function string_reverse(_str) {
	var _outstr = "";
	var _n = string_length(_str);
	for (var _i=_n; _i>=1; _i--) {
		_outstr = _outstr + string_char_at(_str, _i);	
	}
	return _outstr;
}

function string_to_list() {
	if (argument_count == 0) {		
		throw ("String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	if (argument_count >= 2) {		
		var _separator = argument[1];
		if (string_length(_separator) != 1) {
			_separator = ",";
		}
	}
	else {
		var _separator = ",";
	}
	if (argument_count >= 3) {
		var _remove_lead_trail_spaces = argument[2];
	}
	else {
		var _remove_lead_trail_spaces = false;
	}
	
		
	// Process and split
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = string_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}

function string_lrtrim(_string) {
	
	var _str = argument[0];
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;
	
}
