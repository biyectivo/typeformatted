{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Another Typewriter",
  "styleName": "Regular",
  "size": 96.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 5,
  "glyphs": {
    "32": {"x":2,"y":2,"w":64,"h":181,"character":32,"shift":64,"offset":0,},
    "33": {"x":547,"y":368,"w":23,"h":181,"character":33,"shift":25,"offset":1,},
    "34": {"x":491,"y":368,"w":54,"h":181,"character":34,"shift":55,"offset":1,},
    "35": {"x":412,"y":368,"w":77,"h":181,"character":35,"shift":64,"offset":4,},
    "36": {"x":342,"y":368,"w":68,"h":181,"character":36,"shift":70,"offset":1,},
    "37": {"x":257,"y":368,"w":83,"h":181,"character":37,"shift":85,"offset":1,},
    "38": {"x":178,"y":368,"w":77,"h":181,"character":38,"shift":79,"offset":1,},
    "39": {"x":155,"y":368,"w":21,"h":181,"character":39,"shift":64,"offset":14,},
    "40": {"x":123,"y":368,"w":30,"h":181,"character":40,"shift":32,"offset":1,},
    "41": {"x":92,"y":368,"w":29,"h":181,"character":41,"shift":31,"offset":1,},
    "44": {"x":572,"y":368,"w":23,"h":181,"character":44,"shift":25,"offset":1,},
    "46": {"x":70,"y":368,"w":20,"h":181,"character":46,"shift":22,"offset":1,},
    "47": {"x":1922,"y":185,"w":71,"h":181,"character":47,"shift":73,"offset":1,},
    "48": {"x":1855,"y":185,"w":65,"h":181,"character":48,"shift":67,"offset":1,},
    "49": {"x":1792,"y":185,"w":61,"h":181,"character":49,"shift":63,"offset":1,},
    "50": {"x":1728,"y":185,"w":62,"h":181,"character":50,"shift":64,"offset":1,},
    "51": {"x":1665,"y":185,"w":61,"h":181,"character":51,"shift":63,"offset":1,},
    "52": {"x":1598,"y":185,"w":65,"h":181,"character":52,"shift":67,"offset":1,},
    "53": {"x":1538,"y":185,"w":58,"h":181,"character":53,"shift":60,"offset":1,},
    "54": {"x":1476,"y":185,"w":60,"h":181,"character":54,"shift":62,"offset":1,},
    "55": {"x":1419,"y":185,"w":55,"h":181,"character":55,"shift":57,"offset":1,},
    "56": {"x":2,"y":368,"w":66,"h":181,"character":56,"shift":68,"offset":1,},
    "57": {"x":660,"y":368,"w":59,"h":181,"character":57,"shift":61,"offset":1,},
    "58": {"x":1403,"y":368,"w":20,"h":181,"character":58,"shift":22,"offset":1,},
    "59": {"x":721,"y":368,"w":23,"h":181,"character":59,"shift":25,"offset":1,},
    "60": {"x":63,"y":551,"w":59,"h":181,"character":60,"shift":77,"offset":1,},
    "62": {"x":2,"y":551,"w":59,"h":181,"character":62,"shift":77,"offset":1,},
    "63": {"x":1951,"y":368,"w":51,"h":181,"character":63,"shift":53,"offset":1,},
    "65": {"x":1873,"y":368,"w":76,"h":181,"character":65,"shift":78,"offset":1,},
    "66": {"x":1797,"y":368,"w":74,"h":181,"character":66,"shift":76,"offset":1,},
    "67": {"x":1735,"y":368,"w":60,"h":181,"character":67,"shift":62,"offset":1,},
    "68": {"x":1657,"y":368,"w":76,"h":181,"character":68,"shift":77,"offset":1,},
    "69": {"x":1579,"y":368,"w":76,"h":181,"character":69,"shift":78,"offset":1,},
    "70": {"x":1503,"y":368,"w":74,"h":181,"character":70,"shift":76,"offset":1,},
    "71": {"x":124,"y":551,"w":70,"h":181,"character":71,"shift":72,"offset":1,},
    "72": {"x":1425,"y":368,"w":76,"h":181,"character":72,"shift":78,"offset":1,},
    "73": {"x":1345,"y":368,"w":56,"h":181,"character":73,"shift":58,"offset":1,},
    "74": {"x":1273,"y":368,"w":70,"h":181,"character":74,"shift":72,"offset":1,},
    "75": {"x":1194,"y":368,"w":77,"h":181,"character":75,"shift":78,"offset":1,},
    "76": {"x":1118,"y":368,"w":74,"h":181,"character":76,"shift":76,"offset":1,},
    "77": {"x":1040,"y":368,"w":76,"h":181,"character":77,"shift":78,"offset":1,},
    "78": {"x":961,"y":368,"w":77,"h":181,"character":78,"shift":78,"offset":1,},
    "79": {"x":892,"y":368,"w":67,"h":181,"character":79,"shift":70,"offset":1,},
    "80": {"x":818,"y":368,"w":72,"h":181,"character":80,"shift":74,"offset":1,},
    "81": {"x":746,"y":368,"w":70,"h":181,"character":81,"shift":71,"offset":1,},
    "82": {"x":1340,"y":185,"w":77,"h":181,"character":82,"shift":78,"offset":1,},
    "83": {"x":1276,"y":185,"w":62,"h":181,"character":83,"shift":64,"offset":1,},
    "84": {"x":1210,"y":185,"w":64,"h":181,"character":84,"shift":66,"offset":1,},
    "85": {"x":354,"y":185,"w":76,"h":181,"character":85,"shift":78,"offset":1,},
    "86": {"x":1222,"y":2,"w":75,"h":181,"character":86,"shift":77,"offset":1,},
    "87": {"x":1145,"y":2,"w":75,"h":181,"character":87,"shift":77,"offset":1,},
    "88": {"x":1068,"y":2,"w":75,"h":181,"character":88,"shift":77,"offset":1,},
    "89": {"x":990,"y":2,"w":76,"h":181,"character":89,"shift":78,"offset":1,},
    "90": {"x":922,"y":2,"w":66,"h":181,"character":90,"shift":68,"offset":1,},
    "91": {"x":897,"y":2,"w":23,"h":181,"character":91,"shift":64,"offset":18,},
    "92": {"x":824,"y":2,"w":71,"h":181,"character":92,"shift":73,"offset":1,},
    "96": {"x":797,"y":2,"w":25,"h":181,"character":96,"shift":27,"offset":1,},
    "97": {"x":726,"y":2,"w":69,"h":181,"character":97,"shift":71,"offset":1,},
    "98": {"x":653,"y":2,"w":71,"h":181,"character":98,"shift":73,"offset":1,},
    "99": {"x":596,"y":2,"w":55,"h":181,"character":99,"shift":56,"offset":1,},
    "100": {"x":524,"y":2,"w":70,"h":181,"character":100,"shift":72,"offset":1,},
    "101": {"x":465,"y":2,"w":57,"h":181,"character":101,"shift":59,"offset":1,},
    "102": {"x":397,"y":2,"w":66,"h":181,"character":102,"shift":68,"offset":1,},
    "103": {"x":327,"y":2,"w":68,"h":181,"character":103,"shift":70,"offset":1,},
    "104": {"x":250,"y":2,"w":75,"h":181,"character":104,"shift":77,"offset":1,},
    "105": {"x":194,"y":2,"w":54,"h":181,"character":105,"shift":55,"offset":1,},
    "106": {"x":144,"y":2,"w":48,"h":181,"character":106,"shift":50,"offset":1,},
    "107": {"x":68,"y":2,"w":74,"h":181,"character":107,"shift":76,"offset":1,},
    "108": {"x":1299,"y":2,"w":61,"h":181,"character":108,"shift":63,"offset":1,},
    "109": {"x":1362,"y":2,"w":76,"h":181,"character":109,"shift":78,"offset":1,},
    "110": {"x":1440,"y":2,"w":70,"h":181,"character":110,"shift":72,"offset":1,},
    "111": {"x":1512,"y":2,"w":61,"h":181,"character":111,"shift":62,"offset":1,},
    "112": {"x":1061,"y":185,"w":71,"h":181,"character":112,"shift":73,"offset":1,},
    "113": {"x":989,"y":185,"w":70,"h":181,"character":113,"shift":72,"offset":1,},
    "114": {"x":921,"y":185,"w":66,"h":181,"character":114,"shift":68,"offset":1,},
    "115": {"x":865,"y":185,"w":54,"h":181,"character":115,"shift":56,"offset":1,},
    "116": {"x":802,"y":185,"w":61,"h":181,"character":116,"shift":63,"offset":1,},
    "117": {"x":726,"y":185,"w":74,"h":181,"character":117,"shift":76,"offset":1,},
    "118": {"x":649,"y":185,"w":75,"h":181,"character":118,"shift":76,"offset":1,},
    "119": {"x":572,"y":185,"w":75,"h":181,"character":119,"shift":77,"offset":1,},
    "120": {"x":494,"y":185,"w":76,"h":181,"character":120,"shift":78,"offset":1,},
    "121": {"x":1134,"y":185,"w":74,"h":181,"character":121,"shift":76,"offset":1,},
    "122": {"x":432,"y":185,"w":60,"h":181,"character":122,"shift":62,"offset":1,},
    "162": {"x":272,"y":185,"w":80,"h":181,"character":162,"shift":82,"offset":1,},
    "196": {"x":194,"y":185,"w":76,"h":181,"character":196,"shift":78,"offset":1,},
    "197": {"x":116,"y":185,"w":76,"h":181,"character":197,"shift":78,"offset":1,},
    "198": {"x":2,"y":185,"w":112,"h":181,"character":198,"shift":114,"offset":1,},
    "214": {"x":1872,"y":2,"w":67,"h":181,"character":214,"shift":70,"offset":1,},
    "216": {"x":1794,"y":2,"w":76,"h":181,"character":216,"shift":78,"offset":1,},
    "223": {"x":1717,"y":2,"w":75,"h":181,"character":223,"shift":77,"offset":1,},
    "228": {"x":1646,"y":2,"w":69,"h":181,"character":228,"shift":71,"offset":1,},
    "229": {"x":1575,"y":2,"w":69,"h":181,"character":229,"shift":71,"offset":1,},
    "246": {"x":597,"y":368,"w":61,"h":181,"character":246,"shift":62,"offset":1,},
    "248": {"x":196,"y":551,"w":62,"h":181,"character":248,"shift":64,"offset":1,},
  },
  "kerningPairs": [
    {"first":49,"second":50,"amount":5,},
    {"first":50,"second":51,"amount":5,},
    {"first":51,"second":52,"amount":5,},
    {"first":52,"second":53,"amount":5,},
    {"first":52,"second":97,"amount":5,},
    {"first":53,"second":54,"amount":5,},
    {"first":54,"second":55,"amount":5,},
    {"first":55,"second":56,"amount":5,},
    {"first":56,"second":57,"amount":5,},
    {"first":57,"second":48,"amount":5,},
    {"first":65,"second":65,"amount":5,},
    {"first":65,"second":66,"amount":5,},
    {"first":65,"second":71,"amount":5,},
    {"first":65,"second":72,"amount":5,},
    {"first":65,"second":77,"amount":5,},
    {"first":65,"second":79,"amount":5,},
    {"first":65,"second":83,"amount":5,},
    {"first":66,"second":67,"amount":5,},
    {"first":66,"second":85,"amount":5,},
    {"first":67,"second":97,"amount":5,},
    {"first":68,"second":69,"amount":5,},
    {"first":69,"second":69,"amount":5,},
    {"first":69,"second":70,"amount":5,},
    {"first":69,"second":79,"amount":5,},
    {"first":69,"second":82,"amount":5,},
    {"first":69,"second":83,"amount":5,},
    {"first":69,"second":86,"amount":5,},
    {"first":70,"second":71,"amount":5,},
    {"first":71,"second":69,"amount":5,},
    {"first":71,"second":72,"amount":5,},
    {"first":71,"second":79,"amount":5,},
    {"first":71,"second":82,"amount":5,},
    {"first":72,"second":65,"amount":5,},
    {"first":72,"second":69,"amount":5,},
    {"first":72,"second":72,"amount":5,},
    {"first":72,"second":79,"amount":5,},
    {"first":77,"second":66,"amount":5,},
    {"first":77,"second":69,"amount":5,},
    {"first":78,"second":65,"amount":5,},
    {"first":78,"second":83,"amount":5,},
    {"first":79,"second":65,"amount":5,},
    {"first":79,"second":72,"amount":5,},
    {"first":79,"second":78,"amount":5,},
    {"first":79,"second":79,"amount":5,},
    {"first":79,"second":82,"amount":5,},
    {"first":79,"second":85,"amount":5,},
    {"first":82,"second":69,"amount":5,},
    {"first":82,"second":71,"amount":5,},
    {"first":82,"second":83,"amount":5,},
    {"first":82,"second":85,"amount":5,},
    {"first":83,"second":69,"amount":5,},
    {"first":83,"second":72,"amount":5,},
    {"first":85,"second":78,"amount":5,},
    {"first":85,"second":82,"amount":5,},
    {"first":85,"second":83,"amount":5,},
    {"first":86,"second":69,"amount":5,},
    {"first":86,"second":79,"amount":5,},
    {"first":97,"second":98,"amount":5,},
    {"first":97,"second":109,"amount":5,},
    {"first":98,"second":99,"amount":5,},
    {"first":98,"second":117,"amount":5,},
    {"first":99,"second":49,"amount":5,},
    {"first":99,"second":100,"amount":5,},
    {"first":100,"second":101,"amount":5,},
    {"first":101,"second":102,"amount":5,},
    {"first":101,"second":118,"amount":5,},
    {"first":102,"second":103,"amount":5,},
    {"first":103,"second":101,"amount":5,},
    {"first":103,"second":104,"amount":5,},
    {"first":104,"second":105,"amount":5,},
    {"first":105,"second":106,"amount":5,},
    {"first":106,"second":107,"amount":5,},
    {"first":107,"second":108,"amount":5,},
    {"first":108,"second":109,"amount":5,},
    {"first":109,"second":98,"amount":5,},
    {"first":110,"second":111,"amount":5,},
    {"first":110,"second":115,"amount":5,},
    {"first":111,"second":110,"amount":5,},
    {"first":111,"second":112,"amount":5,},
    {"first":112,"second":113,"amount":5,},
    {"first":113,"second":114,"amount":5,},
    {"first":114,"second":103,"amount":5,},
    {"first":114,"second":115,"amount":5,},
    {"first":115,"second":116,"amount":5,},
    {"first":116,"second":117,"amount":5,},
    {"first":117,"second":114,"amount":5,},
    {"first":117,"second":118,"amount":5,},
    {"first":118,"second":111,"amount":5,},
    {"first":118,"second":119,"amount":5,},
    {"first":119,"second":120,"amount":5,},
    {"first":120,"second":121,"amount":5,},
    {"first":121,"second":122,"amount":5,},
  ],
  "ranges": [
    {"lower":32,"upper":255,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/TypeFormatted Example Project/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_TypeFormatted",
  "tags": [],
  "resourceType": "GMFont",
}