{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Consolas",
  "styleName": "Regular",
  "size": 96.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":70,"h":150,"character":32,"shift":70,"offset":0,},
    "33": {"x":849,"y":458,"w":18,"h":150,"character":33,"shift":70,"offset":26,},
    "34": {"x":808,"y":458,"w":39,"h":150,"character":34,"shift":70,"offset":16,},
    "35": {"x":740,"y":458,"w":66,"h":150,"character":35,"shift":70,"offset":2,},
    "36": {"x":681,"y":458,"w":57,"h":150,"character":36,"shift":70,"offset":6,},
    "37": {"x":610,"y":458,"w":69,"h":150,"character":37,"shift":70,"offset":1,},
    "38": {"x":540,"y":458,"w":68,"h":150,"character":38,"shift":70,"offset":3,},
    "39": {"x":523,"y":458,"w":15,"h":150,"character":39,"shift":70,"offset":28,},
    "40": {"x":484,"y":458,"w":37,"h":150,"character":40,"shift":70,"offset":18,},
    "41": {"x":446,"y":458,"w":36,"h":150,"character":41,"shift":70,"offset":16,},
    "42": {"x":869,"y":458,"w":51,"h":150,"character":42,"shift":70,"offset":10,},
    "43": {"x":383,"y":458,"w":61,"h":150,"character":43,"shift":70,"offset":5,},
    "44": {"x":297,"y":458,"w":32,"h":150,"character":44,"shift":70,"offset":14,},
    "45": {"x":257,"y":458,"w":38,"h":150,"character":45,"shift":70,"offset":16,},
    "46": {"x":233,"y":458,"w":22,"h":150,"character":46,"shift":70,"offset":24,},
    "47": {"x":177,"y":458,"w":54,"h":150,"character":47,"shift":70,"offset":7,},
    "48": {"x":115,"y":458,"w":60,"h":150,"character":48,"shift":70,"offset":5,},
    "49": {"x":58,"y":458,"w":55,"h":150,"character":49,"shift":70,"offset":8,},
    "50": {"x":2,"y":458,"w":54,"h":150,"character":50,"shift":70,"offset":9,},
    "51": {"x":921,"y":306,"w":52,"h":150,"character":51,"shift":70,"offset":10,},
    "52": {"x":853,"y":306,"w":66,"h":150,"character":52,"shift":70,"offset":2,},
    "53": {"x":331,"y":458,"w":50,"h":150,"character":53,"shift":70,"offset":11,},
    "54": {"x":922,"y":458,"w":57,"h":150,"character":54,"shift":70,"offset":7,},
    "55": {"x":2,"y":610,"w":56,"h":150,"character":55,"shift":70,"offset":7,},
    "56": {"x":60,"y":610,"w":56,"h":150,"character":56,"shift":70,"offset":7,},
    "57": {"x":217,"y":762,"w":57,"h":150,"character":57,"shift":70,"offset":6,},
    "58": {"x":195,"y":762,"w":20,"h":150,"character":58,"shift":70,"offset":25,},
    "59": {"x":162,"y":762,"w":31,"h":150,"character":59,"shift":70,"offset":15,},
    "60": {"x":110,"y":762,"w":50,"h":150,"character":60,"shift":70,"offset":8,},
    "61": {"x":53,"y":762,"w":55,"h":150,"character":61,"shift":70,"offset":8,},
    "62": {"x":2,"y":762,"w":49,"h":150,"character":62,"shift":70,"offset":13,},
    "63": {"x":935,"y":610,"w":40,"h":150,"character":63,"shift":70,"offset":18,},
    "64": {"x":863,"y":610,"w":70,"h":150,"character":64,"shift":70,"offset":0,},
    "65": {"x":791,"y":610,"w":70,"h":150,"character":65,"shift":70,"offset":0,},
    "66": {"x":734,"y":610,"w":55,"h":150,"character":66,"shift":70,"offset":9,},
    "67": {"x":674,"y":610,"w":58,"h":150,"character":67,"shift":70,"offset":5,},
    "68": {"x":612,"y":610,"w":60,"h":150,"character":68,"shift":70,"offset":6,},
    "69": {"x":563,"y":610,"w":47,"h":150,"character":69,"shift":70,"offset":12,},
    "70": {"x":514,"y":610,"w":47,"h":150,"character":70,"shift":70,"offset":12,},
    "71": {"x":452,"y":610,"w":60,"h":150,"character":71,"shift":70,"offset":4,},
    "72": {"x":392,"y":610,"w":58,"h":150,"character":72,"shift":70,"offset":6,},
    "73": {"x":340,"y":610,"w":50,"h":150,"character":73,"shift":70,"offset":10,},
    "74": {"x":294,"y":610,"w":44,"h":150,"character":74,"shift":70,"offset":11,},
    "75": {"x":235,"y":610,"w":57,"h":150,"character":75,"shift":70,"offset":9,},
    "76": {"x":185,"y":610,"w":48,"h":150,"character":76,"shift":70,"offset":14,},
    "77": {"x":118,"y":610,"w":65,"h":150,"character":77,"shift":70,"offset":3,},
    "78": {"x":795,"y":306,"w":56,"h":150,"character":78,"shift":70,"offset":7,},
    "79": {"x":729,"y":306,"w":64,"h":150,"character":79,"shift":70,"offset":3,},
    "80": {"x":672,"y":306,"w":55,"h":150,"character":80,"shift":70,"offset":9,},
    "81": {"x":348,"y":154,"w":68,"h":150,"character":81,"shift":70,"offset":3,},
    "82": {"x":256,"y":154,"w":56,"h":150,"character":82,"shift":70,"offset":10,},
    "83": {"x":197,"y":154,"w":57,"h":150,"character":83,"shift":70,"offset":6,},
    "84": {"x":135,"y":154,"w":60,"h":150,"character":84,"shift":70,"offset":5,},
    "85": {"x":75,"y":154,"w":58,"h":150,"character":85,"shift":70,"offset":6,},
    "86": {"x":2,"y":154,"w":71,"h":150,"character":86,"shift":70,"offset":0,},
    "87": {"x":888,"y":2,"w":66,"h":150,"character":87,"shift":70,"offset":2,},
    "88": {"x":818,"y":2,"w":68,"h":150,"character":88,"shift":70,"offset":1,},
    "89": {"x":745,"y":2,"w":71,"h":150,"character":89,"shift":70,"offset":0,},
    "90": {"x":685,"y":2,"w":58,"h":150,"character":90,"shift":70,"offset":6,},
    "91": {"x":314,"y":154,"w":32,"h":150,"character":91,"shift":70,"offset":21,},
    "92": {"x":629,"y":2,"w":54,"h":150,"character":92,"shift":70,"offset":10,},
    "93": {"x":528,"y":2,"w":31,"h":150,"character":93,"shift":70,"offset":18,},
    "94": {"x":469,"y":2,"w":57,"h":150,"character":94,"shift":70,"offset":7,},
    "95": {"x":396,"y":2,"w":71,"h":150,"character":95,"shift":70,"offset":0,},
    "96": {"x":352,"y":2,"w":42,"h":150,"character":96,"shift":70,"offset":0,},
    "97": {"x":297,"y":2,"w":53,"h":150,"character":97,"shift":70,"offset":8,},
    "98": {"x":241,"y":2,"w":54,"h":150,"character":98,"shift":70,"offset":10,},
    "99": {"x":189,"y":2,"w":50,"h":150,"character":99,"shift":70,"offset":9,},
    "100": {"x":132,"y":2,"w":55,"h":150,"character":100,"shift":70,"offset":6,},
    "101": {"x":74,"y":2,"w":56,"h":150,"character":101,"shift":70,"offset":7,},
    "102": {"x":561,"y":2,"w":66,"h":150,"character":102,"shift":70,"offset":0,},
    "103": {"x":418,"y":154,"w":61,"h":150,"character":103,"shift":70,"offset":5,},
    "104": {"x":2,"y":306,"w":51,"h":150,"character":104,"shift":70,"offset":10,},
    "105": {"x":481,"y":154,"w":52,"h":150,"character":105,"shift":70,"offset":10,},
    "106": {"x":572,"y":306,"w":47,"h":150,"character":106,"shift":70,"offset":8,},
    "107": {"x":514,"y":306,"w":56,"h":150,"character":107,"shift":70,"offset":11,},
    "108": {"x":460,"y":306,"w":52,"h":150,"character":108,"shift":70,"offset":10,},
    "109": {"x":398,"y":306,"w":60,"h":150,"character":109,"shift":70,"offset":5,},
    "110": {"x":345,"y":306,"w":51,"h":150,"character":110,"shift":70,"offset":10,},
    "111": {"x":283,"y":306,"w":60,"h":150,"character":111,"shift":70,"offset":5,},
    "112": {"x":227,"y":306,"w":54,"h":150,"character":112,"shift":70,"offset":10,},
    "113": {"x":170,"y":306,"w":55,"h":150,"character":113,"shift":70,"offset":6,},
    "114": {"x":115,"y":306,"w":53,"h":150,"character":114,"shift":70,"offset":12,},
    "115": {"x":621,"y":306,"w":49,"h":150,"character":115,"shift":70,"offset":11,},
    "116": {"x":55,"y":306,"w":58,"h":150,"character":116,"shift":70,"offset":3,},
    "117": {"x":962,"y":154,"w":51,"h":150,"character":117,"shift":70,"offset":10,},
    "118": {"x":898,"y":154,"w":62,"h":150,"character":118,"shift":70,"offset":4,},
    "119": {"x":829,"y":154,"w":67,"h":150,"character":119,"shift":70,"offset":2,},
    "120": {"x":764,"y":154,"w":63,"h":150,"character":120,"shift":70,"offset":4,},
    "121": {"x":699,"y":154,"w":63,"h":150,"character":121,"shift":70,"offset":3,},
    "122": {"x":646,"y":154,"w":51,"h":150,"character":122,"shift":70,"offset":10,},
    "123": {"x":597,"y":154,"w":47,"h":150,"character":123,"shift":70,"offset":10,},
    "124": {"x":584,"y":154,"w":11,"h":150,"character":124,"shift":70,"offset":30,},
    "125": {"x":535,"y":154,"w":47,"h":150,"character":125,"shift":70,"offset":14,},
    "126": {"x":276,"y":762,"w":63,"h":150,"character":126,"shift":70,"offset":4,},
    "9647": {"x":341,"y":762,"w":74,"h":150,"character":9647,"shift":124,"offset":25,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/TypeFormatted Example Project/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_Consolas",
  "tags": [],
  "resourceType": "GMFont",
}